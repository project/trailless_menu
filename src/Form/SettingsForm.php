<?php

namespace Drupal\trailless_menu\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\trailless_menu\Dictionary\TraillessMenus;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The settings form for the Trailless Menu module.
 */
class SettingsForm extends ConfigFormBase {

  protected const CONFIG_NAME = TraillessMenus::CONFIG_NAME;

  protected const TRAILLESS_MENUS_SETTING = TraillessMenus::TRAILLESS_MENUS_SETTING;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The cache tags invalidator to be used.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);

    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->cacheTagsInvalidator = $container->get('cache_tags.invalidator');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'trailless_menu_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [static::CONFIG_NAME];
  }

  /**
   * Returns options for a menu checkboxes.
   */
  protected function getMenuOptions(): array {
    $menus = $this->entityTypeManager->getStorage('menu')
      ->loadMultiple();

    // Sort the entities using the entity class's sort() method.
    // See \Drupal\Core\Config\Entity\ConfigEntityBase::sort().
    // @todo Remove dependency on non-BC method sort() here.
    $menu_entity_type = $this->entityTypeManager->getDefinition('menu');
    uasort($menus, [$menu_entity_type->getClass(), 'sort']);

    $result = [];
    foreach ($menus as $menu) {
      $result[$menu->id()] = $menu->label();
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config(static::CONFIG_NAME);

    $trailless_menus = $config->get(static::TRAILLESS_MENUS_SETTING) ?? [];
    $trailless_menus = array_keys($trailless_menus);

    $form[static::TRAILLESS_MENUS_SETTING] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Trailless menus'),
      '#description' => $this->t("Selected menus don't have an active trail, which means parents of the active item and the active item itself don't receive special HTML class in these menus, children of the active menu item aren't expanded, etc."),
      '#options' => $this->getMenuOptions(),
      '#default_value' => $trailless_menus,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(static::CONFIG_NAME);

    $trailless_menus = $form_state->getValue(static::TRAILLESS_MENUS_SETTING) ?? [];
    $trailless_menus = array_filter($trailless_menus);
    sort($trailless_menus);
    $trailless_menu_map = array_fill_keys($trailless_menus, TRUE);

    $existing_trailless_menu = $config->get(static::TRAILLESS_MENUS_SETTING);
    $config->set(static::TRAILLESS_MENUS_SETTING, $trailless_menu_map);
    $config->save();

    if ($existing_trailless_menu !== $trailless_menu_map) {
      $diff_array = array_diff_key($existing_trailless_menu, $trailless_menu_map) +
        array_diff_key($trailless_menu_map, $existing_trailless_menu);
      $tags = [];
      foreach ($diff_array as $item => $value) {
        $tags[] = 'config:system.menu.' . $item;
      }
      $this->cacheTagsInvalidator->invalidateTags($tags);
    }

    parent::submitForm($form, $form_state);
  }

}
