<?php

namespace Drupal\trailless_menu\Dictionary;

/**
 * Provides constants for the module.
 */
final class TraillessMenus {

  /**
   * The settings configuration name.
   */
  const CONFIG_NAME = 'trailless_menu.settings';

  /**
   * The setting that contains the map of trailless menus.
   */
  const TRAILLESS_MENUS_SETTING = 'trailless_menus';

}
