<?php

namespace Drupal\trailless_menu\Menu;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DestructableInterface;
use Drupal\Core\Menu\MenuActiveTrailInterface;
use Drupal\trailless_menu\Dictionary\TraillessMenus;

/**
 * Provides a decorator of the menu active trail service for trailless menus.
 *
 * It returns empty active link and active trail for the configured menus.
 */
class TraillessMenuActiveTrail implements MenuActiveTrailInterface, DestructableInterface {

  protected const CONFIG_NAME = TraillessMenus::CONFIG_NAME;

  protected const TRAILLESS_MENUS_SETTING = TraillessMenus::TRAILLESS_MENUS_SETTING;

  /**
   * The decorated service.
   *
   * @var \Drupal\Core\Menu\MenuActiveTrailInterface
   */
  protected $originalService;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The cached map of trailless menu names.
   *
   * @var array|null
   */
  protected $traillessMenuMap;

  /**
   * The constructor.
   */
  public function __construct(
    MenuActiveTrailInterface $original_service,
    ConfigFactoryInterface $config_factory
  ) {
    $this->originalService = $original_service;
    $this->configFactory = $config_factory;
  }

  /**
   * Check if the passed menu is trailless.
   *
   * @param string $menu_name
   *   The menu name.
   *
   * @return bool
   *   TRUE if the menu is trailless, FALSE otherwise.
   */
  protected function isTraillessMenu(string $menu_name): bool {
    if ($this->traillessMenuMap === NULL) {
      $config = $this->configFactory->get(static::CONFIG_NAME);
      $this->traillessMenuMap = $config->get(static::TRAILLESS_MENUS_SETTING) ?? [];
    }

    return isset($this->traillessMenuMap[$menu_name]);
  }

  /**
   * {@inheritdoc}
   */
  public function getActiveTrailIds($menu_name) {
    if ($menu_name !== NULL && $this->isTraillessMenu($menu_name)) {
      return ['' => ''];
    }

    return $this->originalService->getActiveTrailIds($menu_name);
  }

  /**
   * {@inheritdoc}
   */
  public function getActiveLink($menu_name = NULL) {
    if ($menu_name !== NULL && $this->isTraillessMenu($menu_name)) {
      return NULL;
    }

    $result = $this->originalService->getActiveLink($menu_name);

    if ($menu_name === NULL) {
      $menu_name = $result->getMenuName();
      if ($menu_name && $this->isTraillessMenu($menu_name)) {
        return NULL;
      }
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function destruct() {
    if ($this->originalService instanceof DestructableInterface) {
      $this->originalService->destruct();
    }
  }

}
