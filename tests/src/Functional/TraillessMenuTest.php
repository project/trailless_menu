<?php

namespace Drupal\Tests\trailless_menu\Functional;

use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests that trailless menu doesn't expand and render happens only once.
 *
 * @group trailless_menu
 */
class TraillessMenuTest extends BrowserTestBase {

  /**
   * A trailless menu block.
   *
   * @var \Drupal\block\Entity\Block
   */
  protected $block;

  /**
   * A parent menu link.
   *
   * @var \Drupal\menu_link_content\Entity\MenuLinkContent
   */
  protected $firstItem;

  /**
   * A child menu link.
   *
   * @var \Drupal\menu_link_content\Entity\MenuLinkContent
   */
  protected $secondItem;

  /**
   * An editor user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $editor;

  /**
   * An authenticated user to test block caching.
   *
   * @var object
   */
  protected $normalUser;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'system',
    'block',
    'menu_link_content',
    'node',
    'trailless_menu',
    'trailless_menu_render_count_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalCreateContentType([
      'type' => 'page',
      'name' => 'Basic page',
      'display_submitted' => FALSE,
    ]);

    $this->block = $this->drupalPlaceBlock(
      'system_menu_block:main',
      [
        'label' => 'trailless_menu main navigation',
      ]
    );

    $this->editor = $this->drupalCreateUser([
      'access administration pages',
      'administer blocks',
      'administer menu',
      'create page content',
      'administer trailless menu',
    ]);
    $this->drupalLogin($this->editor);

    // Create additional users to test caching modes.
    $this->normalUser = $this->drupalCreateUser();

    // Create nodes.
    $first = $this->drupalCreateNode();
    $second = $this->drupalCreateNode();

    // Create menu links for nodes.
    $this->firstItem = MenuLinkContent::create([
      'link' => [['uri' => 'entity:node/' . $first->id()]],
      'title' => 'first',
      'menu_name' => 'main',
    ]);
    $this->firstItem->save();

    $this->secondItem = MenuLinkContent::create([
      'link' => [['uri' => 'entity:node/' . $second->id()]],
      'title' => 'second',
      'menu_name' => 'main',
      'parent' => 'menu_link_content:' . $this->firstItem->uuid(),
    ]);
    $this->secondItem->save();

    // Goto trailless_menu setting page and tick main menu and save form.
    $edit = [
      'trailless_menus[main]' => TRUE,
    ];
    $this->drupalGet('/admin/config/user-interface/trailless-menu');
    $this->submitForm($edit, 'Save configuration');
  }

  /**
   * Tests that trailless menu doesn't expand.
   */
  public function testTraillessMenuLink() {
    $this->drupalLogin($this->normalUser);

    // Goto a parent menu link and verify that parent link exists on the page.
    $this->drupalGet($this->firstItem->getUrlObject());
    $this->assertSession()
      ->linkByHrefExists($this->firstItem->getUrlObject()->toString());

    // Goto its child and verify that child link doesn't exists on the page.
    $this->drupalGet($this->secondItem->getUrlObject());
    $this->assertSession()
      ->linkByHrefNotExists($this->secondItem->getUrlObject()->toString());
  }

  /**
   * Tests that render happens only once for trailless menu.
   */
  public function testRenderHappensOnceForTraillessMenu() {
    $this->drupalLogin($this->normalUser);
    $state = \Drupal::state();

    // Resetting state cache so that we don't get cache state value.
    $state->resetCache();

    // Getting render counter, which is set from
    // trailless_menu_render_count_test module. Its value is increased
    // every time system_menu_block:main block is rendered.
    $render_counter = $state->get('render_counter', 0);

    // Invalidating cache tags so that it freshly renders the menu block.
    \Drupal::service('cache_tags.invalidator')
      ->invalidateTags(['config:system.menu.main']);

    // Goto a parent menu link and verify that it is not re-rendered.
    $this->drupalGet($this->firstItem->getUrlObject());

    // Resetting state cache so that we don't get cache state value.
    $state->resetCache();

    $this->assertSame(++$render_counter, $state->get('render_counter'));

    // Goto its child and verify that it is not re-rendered.
    $this->drupalGet($this->secondItem->getUrlObject());

    // Resetting state cache so that we don't get cache state value.
    $state->resetCache();

    $this->assertSame($render_counter, $state->get('render_counter'));
  }

  /**
   * Cache Invalidation Test.
   *
   * Tests that cache gets invalidated upon trailless_menu form save if form
   * setting is changed.
   */
  public function testCacheInvalidationOnFormSave() {
    /** @var \Drupal\Core\Cache\DatabaseCacheTagsChecksum $cacheTagChecksum **/
    $cacheTagChecksum = \Drupal::service('cache_tags.invalidator.checksum');

    // Create menu links for footer menu.
    $this->firstItem = MenuLinkContent::create([
      'link' => 'https://example.com',
      'title' => 'footer1',
      'menu_name' => 'footer',
    ]);
    $this->firstItem->save();

    $this->secondItem = MenuLinkContent::create([
      'link' => 'https://example.com/footer2',
      'title' => 'footer2',
      'menu_name' => 'footer',
    ]);
    $this->secondItem->save();

    // Place footer menu on the page.
    $this->block = $this->drupalPlaceBlock(
      'system_menu_block:footer',
      [
        'label' => 'trailless_menu footer navigation',
      ]
    );

    // Goto home page or some other page.
    $this->drupalGet('');

    // Goto trailless menu config page.
    $this->drupalGet('/admin/config/user-interface/trailless-menu');

    // Check cachetag count for tag reset of main and footer.
    $mainMenuCount = $cacheTagChecksum
      ->getCurrentChecksum(['config:system.menu.main']);
    $footerMenuCount = $cacheTagChecksum
      ->getCurrentChecksum(['config:system.menu.footer']);

    // Tick footer in the config page and save config.
    $edit = [
      'trailless_menus[main]' => TRUE,
      'trailless_menus[footer]' => TRUE,
    ];
    $this->submitForm($edit, 'Save configuration');

    // Check that main menu cachetag count is same and for footer is changed.
    $newMainMenuCount = $cacheTagChecksum
      ->getCurrentChecksum(['config:system.menu.main']);
    $newFooterMenuCount = $cacheTagChecksum
      ->getCurrentChecksum(['config:system.menu.footer']);

    $this->assertSame($mainMenuCount, $newMainMenuCount);
    $this->assertNotSame($footerMenuCount, $newFooterMenuCount);

    // Uncheck main menu and save config.
    $this->drupalGet('/admin/config/user-interface/trailless-menu');
    $edit['trailless_menus[main]'] = FALSE;
    $this->submitForm($edit, 'Save configuration');

    // Check that footer menu cachetag count is same and for main menu
    // is changed.
    $mainMenuCount = $newMainMenuCount;
    $footerMenuCount = $newFooterMenuCount;

    $newMainMenuCount = $cacheTagChecksum
      ->getCurrentChecksum(['config:system.menu.main']);
    $newFooterMenuCount = $cacheTagChecksum
      ->getCurrentChecksum(['config:system.menu.footer']);

    $this->assertNotSame($mainMenuCount, $newMainMenuCount);
    $this->assertSame($footerMenuCount, $newFooterMenuCount);
  }

}
